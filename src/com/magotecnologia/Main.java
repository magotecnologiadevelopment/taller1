package com.magotecnologia;

import com.magotecnologia.Elements.Network;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Network myNetwork= new Network();
        while(true) {
            System.out.println("Escriba que desea realizar \n");
            System.out.println("1. Crear nuevo computador \n");
            System.out.println("2. Crear nuevo dispositivo \n");
            System.out.println("3. Crear nuevo canal de comunicación \n");
            System.out.println("4. Mostrar computadores \n");
            System.out.println("5. Mostrar los dispositivo \n");
            System.out.println("6. Mostrar los canales de comunicación \n");
            System.out.println("7. Enviar mensaje \n");
            System.out.println("8. Conectar Computador a Canal \n");
            System.out.println("9. Conectar Dispositivo a Canal \n");
            Scanner scanner= new Scanner(System.in);
            String entrada = scanner.next();
            switch (entrada){
                case "1":
                    myNetwork.CreateComputer();
                    break;
                case "2":
                    myNetwork.CreateDevice();
                    break;
                case "3":
                    myNetwork.CreateChannel();
                    break;
                case "4":
                    myNetwork.ShowComputers();
                    break;
                case "5":
                    myNetwork.ShowDevices();
                    break;
                case "6":
                    myNetwork.ShowChannels();
                    break;
                case "7":
                    myNetwork.sendMessage();
                    break;
                case "8":
                    myNetwork.ConnectComputerToChannel();
                    break;
                case "9":
                    myNetwork.ConnectDeviceToChannel();
                    break;
                default:
                    System.out.println("Opción no reconocida \n");
                    break;
            }
        }
    }


}

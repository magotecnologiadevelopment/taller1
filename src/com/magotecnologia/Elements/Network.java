package com.magotecnologia.Elements;

import com.magotecnologia.Elements.Devices.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Network {
    private List<Computer> computerList;
    private List<CommDevice> commDeviceList;
    private List<Channel> channelList;
    private List<Message> messageList;

    public void CreateComputer(){
        Computer creatingComputer= new Computer();
        System.out.println("Creando computador \n");
        System.out.println("IP asignada:"+ creatingComputer.getIp() );
        System.out.println("Desea conectarlo a un canal de comunicaciòn? Y/N ");
        Scanner scanner= new Scanner(System.in);
        String entrada = scanner.next();
        switch (entrada){
            case "Y":
                if(channelList != null){
                    System.out.println("Canales de comunicación disponibles");
                    ShowChannels();
                    System.out.println("Seleccione el canal de comunicación al cual desea conectar escribiendo su ID");
                    Scanner scanning = new Scanner(System.in);
                    String entradaCanal = scanning.next();
                    Channel channelToConnect = channelList.stream().filter(channel -> Integer.parseInt(entradaCanal) == channel.getId()).findAny().orElse(null);
                    if(channelToConnect!= null) {
                        connectComputer(creatingComputer,channelToConnect );
                    }
                    else {
                        System.out.println("ID de Canal no encontrado");
                    }
                }
                else {
                    System.out.println("No hay canales disponibles para la conexion");
                }
                break;
            case "N":
                System.out.println("No se podrá comunicar sin asignarsele un canal de comunicación");
                break;
            default:
                System.out.println("Opción no reconocida \n");
                break;
        }
        if(computerList == null){
            computerList= new ArrayList<Computer>();
        }
        computerList.add(creatingComputer);
        System.out.println("La red tiene "+computerList.size() + " computadores" );
    }

    public void ConnectComputerToChannel(){
        if(computerList != null){
            System.out.println("Computadores disponibles");
            ShowComputers();
            System.out.println("Seleccione el computador el desea conectar escribiendo su IP");
            Scanner scanning = new Scanner(System.in);
            String entradaComputador = scanning.next();
            Computer computerToConnect = computerList.stream().filter(computer -> entradaComputador.equals(computer.getIp())).findAny().orElse(null);
            if(computerToConnect!= null) {
                if(channelList != null) {
                    System.out.println("Canales de comunicación disponibles");
                    ShowChannels();
                    System.out.println("Seleccione el canal de comunicación al cual desea conectar escribiendo su ID");
                    Scanner scanningCanal = new Scanner(System.in);
                    String entradaCanal = scanningCanal.next();
                    Channel channelToConnect = channelList.stream().filter(channel -> Integer.parseInt(entradaCanal) == channel.getId()).findAny().orElse(null);
                    if(channelToConnect!= null) {
                        connectComputer(computerToConnect,channelToConnect );
                    }
                    else {
                        System.out.println("ID de Canal no encontrado");
                    }
                }

            }
            else {
                System.out.println("ID de Canal no encontrado");
            }
        }
        else {
            System.out.println("No hay computadores para la conexion");
        }
    }

    public void ConnectDeviceToChannel(){
        if(commDeviceList != null){
            System.out.println("Dispositivos disponibles");
            ShowDevices();
            System.out.println("Seleccione el dispositivo el desea conectar escribiendo su ID");
            Scanner scanning = new Scanner(System.in);
            String entradaDispositivo = scanning.next();
            CommDevice deviceToConnect = commDeviceList.stream().filter(computer -> entradaDispositivo.equals(computer.getId())).findAny().orElse(null);
            if(deviceToConnect!= null) {
                if(channelList != null) {
                    System.out.println("Canales de comunicación disponibles");
                    ShowChannels();
                    System.out.println("Seleccione el canal de comunicación al cual desea conectar escribiendo su ID");
                    Scanner scanningCanal = new Scanner(System.in);
                    String entradaCanal = scanningCanal.next();
                    Channel channelToConnect = channelList.stream().filter(channel -> Integer.parseInt(entradaCanal) == channel.getId()).findAny().orElse(null);
                    if(channelToConnect!= null) {
                        ConnectDevice (deviceToConnect,channelToConnect );
                    }
                    else {
                        System.out.println("ID de Canal no encontrado");
                    }
                }

            }
            else {
                System.out.println("ID de Canal no encontrado");
            }
        }
        else {
            System.out.println("No hay computadores para la conexion");
        }
    }


    public boolean connectComputer(Computer computerToConnect,Channel channelToConnect){
        System.out.println("Conectando el computador de IP:"+computerToConnect.getIp() + " al canal de ID:" + channelToConnect.getId() );
        //Adiciona el canal de comunicacion al computador
        computerToConnect.setCommChannel(channelToConnect);
        //Adiciona el computador al canal
        channelToConnect.addComputer(computerToConnect);
        return true;
   }

    public void CreateChannel(){
       Channel creatingChannel= new Channel();
       System.out.println("Creando canal \n");
       System.out.println("ID asignada:"+ creatingChannel.getId() );
       System.out.println("Desea conectarle un computador? Y/N ");
       Scanner scanner= new Scanner(System.in);
       String entrada = scanner.next();
       switch (entrada){
           case "Y":
               if(computerList != null){
                   System.out.println("Computadores disponibles");
                   ShowComputers();
                   System.out.println("Seleccione el computador al cual desea conectar escribiendo su IP");
                   Scanner scanning = new Scanner(System.in);
                   String entradaComputer = scanning.next();
                   Computer selectedComputer = computerList.stream().filter(computer -> entradaComputer.equals(computer.getIp())).findAny().orElse(null);
                   if(selectedComputer!= null) {
                       connectComputer(selectedComputer,creatingChannel );
                   }
                   else {
                       System.out.println("IP de Computador no encontrado");
                   }
               }
               else {
                   System.out.println("No hay computadores disponibles para la conexion");
               }
               break;
           case "N":
                 break;
           default:
               System.out.println("Opción no reconocida \n");
               break;
       }
       if(channelList == null){
           channelList= new ArrayList<Channel>();
       }
       channelList.add(creatingChannel);
       System.out.println("La red tiene "+channelList.size() + " canales" );
   }

    public void ShowComputers(){
        System.out.println("Mostrando los computadores de la red \n");
        if(computerList!= null){
            for (Computer computer:computerList){
                System.out.println((computerList.indexOf(computer)+1) + " Computador con IP"+ computer.getIp() );
            }
        }
    }

    public void ShowChannels(){
        System.out.println("Mostrando los canales de la red \n");
        if(channelList != null){
            for (Channel channel:channelList){
                System.out.println(channelList.indexOf(channel) + " Canal de comunicación con ID: "+ channel.getId());
            }
        }
        else {
            System.out.println("No hay canales de comunicaciòn para esta red ");
        }
    }

    public void ShowDevices(){
        System.out.println("Mostrando los dispositivos de la red \n");
        if(commDeviceList != null){
            for (CommDevice device:commDeviceList){
                System.out.println((channelList.indexOf(device)+1)  + " Dispositivo con ID: "+ device.getId());
            }
        }
        else {
            System.out.println("No hay canales de comunicaciòn para esta red ");
        }
    }


    public List<Computer> getComputerList() {
        return computerList;
    }

    public void setComputerList(List<Computer> computerList) {
        this.computerList = computerList;
    }

    public List<CommDevice> getCommDeviceList() {
        return commDeviceList;
    }

    public void setCommDeviceList(List<CommDevice> commDeviceList) {
        this.commDeviceList = commDeviceList;
    }

    public List<Channel> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<Channel> channelList) {
        this.channelList = channelList;
    }

    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }

    public void CreateDevice() {
        CommDevice creatingDevice= null;
        System.out.println("Seleccione el tipo de dispositivo \n");
        System.out.println("1.Router  \n");
        System.out.println("2.Hub  \n");
        System.out.println("3.Repeater \n");
        System.out.println("4.Switch \n");
        System.out.println("5.Bridge \n");
        Scanner scanner= new Scanner(System.in);
        String entrada = scanner.next();
        switch (entrada){
            case "1":
                creatingDevice= new Router();
                break;
            case "2":
                creatingDevice= new Hub();
                break;
            case "3":
                creatingDevice= new Repeater();
                break;
            case "4":
                creatingDevice= new Switch();
                break;
            case "5":
                creatingDevice= new Bridge();
                break;
            default:
                System.out.println("Opción no reconocida \n");
                break;
        }
        if(creatingDevice != null) {
            System.out.println("Desea conectarlo a un canal de comunicaciòn? Y/N ");
            Scanner scannerConnect= new Scanner(System.in);
            String entradaCanalAprobacion = scannerConnect.next();
            switch (entradaCanalAprobacion){
                case "Y":
                    if(channelList != null){
                        System.out.println("Canales de comunicación disponibles");
                        ShowChannels();
                        System.out.println("Seleccione el canal de comunicación al cual desea conectar escribiendo su ID");
                        Scanner scanning = new Scanner(System.in);
                        String entradaCanal = scanning.next();
                        Channel channelToConnect = channelList.stream().filter(channel -> Integer.parseInt(entradaCanal) == channel.getId()).findAny().orElse(null);
                        if(channelToConnect!= null) {
                            ConnectDevice(creatingDevice,channelToConnect );
                        }
                        else {
                            System.out.println("ID de Canal no encontrado");
                        }
                    }
                    else {
                        System.out.println("No hay canales disponibles para la conexion");
                    }
                    break;
                case "N":
                    System.out.println("No se podrá comunicar sin asignarsele un canal de comunicación");
                    break;
                default:
                    System.out.println("Opción no reconocida \n");
                    break;
            }
            System.out.println("Creando dispositivo \n");
            System.out.println("Asignado el ID:" + creatingDevice.getId() + " \n");
            if(commDeviceList == null){
                commDeviceList= new ArrayList<CommDevice>();
            }
            commDeviceList.add(creatingDevice);
            System.out.println("La red tiene "+commDeviceList.size() + " dispositivos" );
        }
    }

    private boolean ConnectDevice(CommDevice deviceToConnect,Channel channelToConnect){
        System.out.println("Conectando el dispositivo con ID:"+deviceToConnect.getId() + " al canal de ID:" + channelToConnect.getId() );
        //Adiciona el canal de comunicacion al computador
        deviceToConnect.AddCommChannel(channelToConnect);
        //Adiciona el computador al canal
        channelToConnect.addDevice(deviceToConnect);
        return true;
    }

    public void sendMessage() {
        if (computerList != null) {
            System.out.println("Computadores disponibles en la red");
            ShowComputers();
            System.out.println("Seleccione el computador desde el cual quiere enviar el mensaje  escribiendo su IP");
            Scanner scanning = new Scanner(System.in);
            String entradaComputer = scanning.next();
            Computer selectedComputer = computerList.stream().filter(computer -> entradaComputer.equals(computer.getIp())).findAny().orElse(null);
            if (selectedComputer != null) {
                if(selectedComputer.getCommChannel() != null) {
                    System.out.println("Escriba la IP de destino");
                    Scanner scanningDestino = new Scanner(System.in);
                    String entradaDestino = scanningDestino.next();
                    System.out.println("Escriba el cuerpo del mensaje");
                    Scanner scanningCuerpo = new Scanner(System.in);
                    String entradaCuerpo = scanningCuerpo.next();
                    Message messageToSent= new Message(entradaCuerpo,selectedComputer.getIp(),entradaDestino);
                    System.out.println("Mensaje enviado por computador con IP "+selectedComputer.getIp());
                    System.out.println("Con destino al computador con IP "+entradaDestino);
                    System.out.println("Contenido del mensaje: "+entradaCuerpo);
                    selectedComputer.postToChannel(messageToSent);

                }else{
                    System.out.println("El computador elegido no esta conectado a ningun canal de red");

                }

                } else {
                System.out.println("IP de Computador no encontrado");
            }
        } else {
            System.out.println("No hay computadores disponibles para la conexion");
        }
    }




}

package com.magotecnologia.Elements.Devices;

import com.magotecnologia.Elements.CommDevice;
import com.magotecnologia.Elements.Message;

import java.util.Random;

public class Router extends CommDevice {


    public Router() {
        Random randomizer = new Random();
        int randomIdentifier=randomizer.nextInt(1000000);
        this.id= "Router"+String.format ("%06d", randomIdentifier);
    }

    @Override
    public void ResentMessage(Message message) {
        try {
            Thread.sleep(1000);
            System.out.println("Enviando mensaje con 1 segundo de retraso");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}

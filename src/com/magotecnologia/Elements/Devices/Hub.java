package com.magotecnologia.Elements.Devices;

import com.magotecnologia.Elements.CommDevice;
import com.magotecnologia.Elements.Message;

import java.util.Random;

public class Hub extends CommDevice {

    @Override
    public void ResentMessage(Message message) {
        try {
            Thread.sleep(2000);
            System.out.println("Enviando mensaje con 2 segundos de retraso");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public Hub() {
        Random randomizer = new Random();
        int randomIdentifier=randomizer.nextInt(1000000);
        this.id= "Hub"+String.format ("%06d", randomIdentifier);
    }
}

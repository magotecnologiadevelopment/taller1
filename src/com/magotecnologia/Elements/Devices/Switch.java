package com.magotecnologia.Elements.Devices;

import com.magotecnologia.Elements.Channel;
import com.magotecnologia.Elements.CommDevice;
import com.magotecnologia.Elements.Message;

import java.util.Random;

public class Switch extends CommDevice {

    public Switch() {
        Random randomizer = new Random();
        int randomIdentifier=randomizer.nextInt(1000000);
        this.id= "Switch"+String.format ("%06d", randomIdentifier);
    }

    @Override
    public void getNewMessage(Message message, Channel senderChannel)  {
        try {
            Thread.sleep(4000);
            System.out.println("Enviando mensaje con 4 segundo de retraso");
            super.getNewMessage(message, senderChannel);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}

package com.magotecnologia.Elements.Devices;

import com.magotecnologia.Elements.CommDevice;
import com.magotecnologia.Elements.Message;

import java.util.Random;

public class Repeater extends CommDevice{


    @Override
    public void ResentMessage(Message message) {
            try {
            Thread.sleep(3000);
            System.out.println("Enviando mensaje con 3 segundos de retraso");
            } catch (InterruptedException e) {
            e.printStackTrace();
            }

    }

    public Repeater() {
        Random randomizer = new Random();
        int randomIdentifier=randomizer.nextInt(1000000);
        this.id= "Repeater"+String.format ("%06d", randomIdentifier);

    }
}

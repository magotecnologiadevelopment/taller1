package com.magotecnologia.Elements.Devices;

import com.magotecnologia.Elements.CommDevice;
import com.magotecnologia.Elements.Message;

import java.util.Random;

public class Bridge extends CommDevice {
    @Override
    public void ResentMessage(Message message) {
        try {
            Thread.sleep(5000);
            System.out.println("Enviando mensaje con 5 segundo de retraso");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public Bridge() {
        Random randomizer = new Random();
        int randomIdentifier=randomizer.nextInt(1000000);
        this.id= "Bridge"+String.format ("%06d", randomIdentifier);

    }
}

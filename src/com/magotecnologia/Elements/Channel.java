package com.magotecnologia.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Channel {
    //Id del canal
    private int id;
    //Lista de computadores conectados
    private List<Computer> connectedComputers;
    //Lista de dispositivos conectados
    private List<CommDevice> connectedDevices;

    public Channel() {
        Random randomId = new Random();
        this.id= randomId.nextInt(1024);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Computer> getConnectedComputers() {
        return connectedComputers;
    }

    public void setConnectedComputers(List<Computer> connectedComputers) {
        this.connectedComputers = connectedComputers;
    }

    public List<CommDevice> getConnectedDevices() {
        return connectedDevices;
    }

    public void setConnectedDevices(List<CommDevice> connectedDevices) {
        this.connectedDevices = connectedDevices;
    }

    public void addComputer(Computer computerToAdd){
        if(connectedComputers== null){
            connectedComputers= new ArrayList<>();
        }
        connectedComputers.add(computerToAdd);
    }

    public void addDevice(CommDevice deviceToAdd){
        if(connectedDevices== null){
            connectedDevices=new ArrayList<>();
        }
        connectedDevices.add(deviceToAdd);
    }

    public void getNewMessageFromPC(Message message,Computer senderComputer ) {
        for (Computer connectedComputer:connectedComputers ) {
            if(senderComputer.getIp()!=connectedComputer.getIp() ) {
                connectedComputer.getNewMessage(message);
            }
        }
        for (CommDevice device:connectedDevices){
            device.getNewMessage(message,this);
        }
    }

    public void getNewMessageFromDevice(Message message,CommDevice senderDevice ) {
        for (Computer connectedComputer:connectedComputers ) {
                connectedComputer.getNewMessage(message);
        }
        for (CommDevice device:connectedDevices){
            if(senderDevice.getId()!=device.getId())
            device.getNewMessage(message,this);
        }
    }
}

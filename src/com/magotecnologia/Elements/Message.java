package com.magotecnologia.Elements;

import java.util.ArrayList;
import java.util.List;

public class Message {
    //Cuerpo del mensaje
    private String body;
    //Id del dispositivo que envío el mensaje
    private String sender;
    //Id del dispositivo objetivo
    private String receiver;
    //Dispositivos por los que pasa
    private List<CommDevice> interDevices;
    private boolean received;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public List<CommDevice> getInterDevices() {
        return interDevices;
    }

    public void setInterDevices(List<CommDevice> interDevices) {
        this.interDevices = interDevices;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    public Message(String body, String sender, String receiver) {
        this.body = body;
        this.sender = sender;
        this.receiver = receiver;
    }

    public void addInterDevice(CommDevice deviceToAdd){
        if(interDevices== null){
            interDevices= new ArrayList<>();
        }
        interDevices.add(deviceToAdd);
    }
}

package com.magotecnologia.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CommDevice {
    //Id del dispositivo
    protected String id;
    //Lista de canales conectados
    private List<Channel> commChannel;

    public List<Channel> getCommChannel() {
        return commChannel;
    }

    public void setCommChannel(List<Channel> commChannel) {
        this.commChannel = commChannel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void ResentMessage(Message message)
    {
        System.out.println("Enviando mensaje de inmediato");
    }

    public void AddCommChannel(Channel channelToAdd){
        if(commChannel== null ) {
            commChannel = new ArrayList<>();
        }
        commChannel.add(channelToAdd);
    }


    public void getNewMessage(Message message, Channel channelSender) {
        for (Channel canal:commChannel) {
            if(channelSender.getId() !=  canal.getId()) {
                canal.getNewMessageFromDevice(message,this);
            }
        }
    }
}

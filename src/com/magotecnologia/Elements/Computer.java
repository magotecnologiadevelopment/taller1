package com.magotecnologia.Elements;

import java.util.Random;

public class Computer{
    //Ip del computador que envía
    private String Ip;
    //Canal de comunicación al que esta conectado
    private Channel commChannel;

    public Computer(String ip) {
        Ip = ip;
    }

    public String getIp() {
        return Ip;
    }

    public void setIp(String ip) {
        Ip = ip;
    }

    public Computer() {
        Ip=getRandomIP();
    }

    public Channel getCommChannel() {
        return commChannel;
    }

    public void setCommChannel(Channel commChannel) {
        this.commChannel = commChannel;
    }

    private String getRandomIP(){
        String randomIp;
        StringBuilder builderIp= new StringBuilder();
        for(int i=0;i<4;i++){
            Random randomSegment = new Random();
            int ipSegment= randomSegment.nextInt(256);
            String ipSegmentString=String.valueOf(ipSegment);
            builderIp.append(ipSegment);
            builderIp.append(".");
        }
        builderIp.setLength(builderIp.length() - 1);
        return builderIp.toString();
    }

    public void postToChannel(Message messageToPost){
        commChannel.getNewMessageFromPC(messageToPost, this);
    }


    public void getNewMessage(Message message) {
        if(message.getReceiver().equals("*")|| message.getReceiver().equals(this.Ip))
        {
            System.out.println("Mensaje recibido por computador con IP:"+this.Ip );
            System.out.println("El mensaje lo envió el computador con IP"+message.getSender());
            if(message.getInterDevices()!= null){
                StringBuilder messageBuilder= new StringBuilder();
                messageBuilder.append("El mensaje pasó por los siguientes dispositivos:");
                for (CommDevice interDevice:message.getInterDevices()) {
                    messageBuilder.append(interDevice.getId());
                }
            }
            System.out.println("Contenido"+message.getBody());
        }
    }
}
